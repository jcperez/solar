## Albano Gonzalez, Juan C. Perez
## Grupo de Observación de la Tierra y la Atmósfera (GOTA)
## University of La Laguna
## This software is supplied "AS IS" without any warranties and support.
## Authors reserves the right to make changes in the software without notification

# Based on Wilks,1997, Resampling hypothesis test for autocorrelated fields, J Climate
import numpy as np
import sys
import math

def Theta(a,b,c):
#Moving average parameter for the ARMA(1,1) model
#a,b,c contstants of the quadratic equation
	#print "a: %f, b: %f, c: %f" %(a,b,c) 
	d = b**2-4*a*c # discriminant

	if d < 0.0005:
    		print ("This equation has no real solution")
		return 0.0
	elif d == 0:
    		x1 = (-b+math.sqrt(b**2-4*a*c))/2*a
	else:
		x1 = (-b+math.sqrt((b**2)-(4*(a*c))))/(2*a)
		x2 = (-b-math.sqrt((b**2)-(4*(a*c))))/(2*a)
	if math.fabs(x1) < 1.0:
		return x1
	elif math.fabs(x2) < 1.0:
		return x2
	else:
		print ("This equation has no solution with abs(theta) < 1")
		return 0.0

def V(rk):
# Variance inflaction factor
# Estimates of the autocorrelations at lags k: rk, k =1,2,.., n-1 (n sample size)

	k = np.arange(1.0,len(rk)+1.0)
	v = 1.0 +2.0*np.sum((1.0-k/(len(k)+1.0))*rk)
	return v

def r1(x):
# Lag 1 autocorrelation r1
# x: time serie
	xmed = np.mean(x)
	r = np.sum((x[1:]-xmed)*(x[:-1]-xmed))/np.sum((x-xmed)*(x-xmed))
	return r

def r2(x):
# Lag 2 autocorrelation r1
# x: time serie
	xm = np.mean(x[2:])
	xM = np.mean(x[:-2])
	r = np.sum((x[2:]-xm)*(x[:-2]-xM))/np.sqrt(np.sum((x[2:]-xm)*(x[2:]-xm))*np.sum((x[:-2]-xM)*(x[:-2]-xM)))
	return r

def Vprima(x, ts_model="ar1"):
# Adjusted variance inflation factor
# time series: x
# Type of model for the time series: ar1, ar2, arma11

	n = len(x)
	rk = np.arange(1.0,1.0*n)
	if ts_model == "ar1":
		rk[0] = r1(x)
		for i in range(1,n-1):
			rk[i] = math.pow(rk[0],i+1)
		v = V(rk)
		vprima = v*math.exp(2.0*v/n)
	elif ts_model == "ar2":
    		##print "Vprima ar2"
		rk[0] = r1(x)
		rk[1] = r2(x)
		phi1 = rk[0]*(1.0-rk[1])/(1.0-rk[0]*rk[0])
		phi2 = (rk[1]-rk[0]*rk[0])/(1.0-rk[0]*rk[0])
		for i in range(2,len(x)-1):
			rk[i]=phi1*rk[i-1]+phi2*rk[i-2]
		v = V(rk)
		vprima = v*math.exp(3.0*v/n)
	elif ts_model == "arma11":
		rk[0] = r1(x)
		rk[1] = r2(x)
		phi1 = rk[1]/rk[0]
		for i in range(2,len(x)-1):
			rk[i]=phi1*rk[i-1]
		v = V(rk)
		theta = Theta(rk[0]-phi1, 1.0-2.0*phi1*rk[0]+phi1*phi1, rk[0]-phi1)
		try: 
			vprima = v*math.exp((2.0+theta)*v/n)
		except:
			vprima = v
	else:
    		print "Vprima: unknown ts_model: %s" %(ts_model)
		sys.exit(-1)
	if vprima < 0.0001:
		vprima = 1.0
	return vprima

def block_length(vprima, n, ts_model="ar1"):
# Adjusted variance inflation factor
# vprima : Adjusted variance inflation factor
# n : sample size (temporal steps)
# Type of model for the time series: ar1, ar2, arma11
	l = math.sqrt(1.0*n)
	la = 0.0
	if ts_model == "ar1":
		while math.fabs(la-l)>1.0:
			la = l
			l = math.pow(n - la + 1.0 , 2.0*(1.0-1.0/vprima)/3.0)
	elif ts_model == "ar2" or ts_model == "arma11":
 		while math.fabs(la-l)>1.0:
			la = l
			l = math.pow(n - la + 1.0 , 2.0*(1.0-1.0/math.sqrt(4.0*vprima))/3.0)
	else:
    		print "block_length: unknown ts_model: %s" %(ts_model)
		sys.exit(-1)
	if math.isnan(l):  #si no se puede resolver l = n**1/3
		l = math.pow(1.0*n, 1.0/3.0)
	return int(math.floor(l))

def two_mean_test(a, b, nbs=2000, ts_model="ar1", alt_hypothesis = "ma_dist_mb", alpha=0.05):
# Series to compare: "a" and "b"
# Number of bootstrap samples: nbs
# Type of model for the time series: ar1 [AR(1)], ar2 [AR(2)], arma11 [ARMA(1,1)]
# Alternative hypothesis: "ma_dist_mb" (mean(a) != mean(b)) ; "ma_lt_mb" (mean(a)<mean(b)) ; "ma_gt_mb" (mean(a) > mean(b))
# alpha value

	S = np.mean(a) - np.mean(b)
	na = len(a)
	nb = len(b)
	vprima_a = Vprima(a, ts_model)
	vprima_b = Vprima(b, ts_model)
	sigma = math.sqrt(vprima_a*np.var(a)/na + vprima_b*np.var(b)/nb)
	d = S/sigma
	la = block_length(vprima_a, na, ts_model)
	lb = block_length(vprima_b, nb, ts_model)
	numb_a = int(math.ceil(1.0*na/la))
	numb_b = int(math.ceil(1.0*nb/lb))
	
	#for every resampling using moving block bootstrap
	lower = 0
	higher = 0
	for i in range(nbs):
		# for serie a: new bootstrap serie
		aa = np.zeros(len(a))
		starts = np.random.randint(0,na-la,numb_a)
		c = 0
		for s in starts[:-1]:
			aa[c:c+la] = a[s:s+la]
			c=c+la
		aa[c:]=a[starts[-1]:starts[-1]+na-c]

		# for serie b: new bootstrap serie
		bb = np.zeros(len(b))
		starts = np.random.randint(0,nb-lb,numb_b)
		c = 0
		for s in starts[:-1]:
			bb[c:c+lb] = b[s:s+lb]
			c=c+lb
		bb[c:]=b[starts[-1]:starts[-1]+nb-c]

		S_bs = np.mean(aa) - np.mean(bb)
		sigma_bs = math.sqrt(vprima_a*np.var(aa)/na + vprima_b*np.var(bb)/nb)
		d_bs = (S_bs-S)/sigma_bs
		if d_bs >= d:
			higher += 1
		if d_bs <= d:
			lower += 1

	p_high = higher*1.0/nbs
	p_low = lower*1.0/nbs

	#Test the hypothesis
	Ho = True
	if alt_hypothesis == "ma_dist_mb":
		if (higher/(nbs+1.0) < alpha/2.0) or (lower/(nbs+1.0) < alpha/2.0):
			Ho = False
		if p_high < p_low:
			p_val = p_high
		else:
			p_val = p_low
	elif alt_hypothesis == "ma_lt_mb":
		if lower/(nbs+1.0) < alpha:
			Ho = False
		p_val = p_low
	elif alt_hypothesis == "ma_gt_mb":
		if higher/(nbs+1.0) < alpha:
			Ho = False
		p_val = p_high
	else:
    		print "two_mean_test: unknown ts_model: %s" %(ts_model)
		sys.exit(-1)

	return Ho, p_val
	




